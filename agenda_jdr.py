import urllib.request
from ics import Calendar, Event
#from dateutil import tz
import arrow
from plugin import Plugin
import re
from datetime import datetime
import dateutil.rrule
import icalendar
from pytz import timezone
localtz = timezone('Europe/Paris')





class AgendaJdr(Plugin):
    """Plugin "Ce soir on joue ?"
    
    Depuis un calendrier ical distant, récupère la date du prochain événement et l'affiche
    avec son titre. À défaut, l'événement le plus récent est utilisé.
    
    Configuration :
        calendar_url : URL du calendrier ical
    """
    def __init__(self):
        help = 'Jdr'
        name = 'agenda_jdr'
        Plugin.__init__(self, name, help)
        self.commands = ['jdr', 'cesoironjoue', 'cesoironjoue?', 'csoj?', 'csoj']

    def call(self, bot, context, *args):
        now = datetime.now(tz=localtz)
        is_next, (last, name) = self.get_next_rpg_session(now)
        if is_next:
            # S'il y a un événement prévu dans le futur
            if last < now:
                # Et qu'il a déjà commencé
                answer = "On joue maintenant ! (" + name + ")."
            #elif last.day == now.day and last.month == now.month and last.year == now.year:
            #    # Et qu'il est aujourd'hui
            #    answer = "Ce soir on joue à " + last.strftime("HH:mm") + " ! (" + name + ")"
            else:
                # Et qu'il est plus tard
                answer = "On joue le " + last.strftime("%d-%m-%Y") + " (" + name + ")."
        else:
            answer = "Il n'y a de session de prévue. La dernière était le " + last.strftime('%d-%m-%Y') + " (" + name + ")."
        # Envoyer la réponse sur le salon de discussion
        bot.connection.privmsg(context['channel'], answer)


    def get_next_rpg_session(self, now):
        """Détermine le prochain événement du calendrier distant

        Récupère le calendrier des sessions de JdR en strftime ics et en récupère le dernier événement

        Args:
            now (datetime): horaire au moment de la requête

        Returns:
            bool, ical_event : Vrai si l'événement est dans le futur, événement à afficher
        """
        timezone = now.tzinfo
        try:
            ical = icalendar.Calendar.from_ical(urllib.request.urlopen(self.config['calendar_url']).read().decode())
            events = []
            past_events = []
            for e in ical.walk():
                if e.name != "VEVENT":
                    continue
                try:
                    if e.get('dtend').dt > now:
                        event = (e.get('dtbegin').dt, e.get('summary'))
                        events.append(event)
                    elif e.get('dtend').dt < now:
                        event = (e.get('dtbegin').dt, e.get('summary'))
                        past_events.append(event)
                except:
                    # dtend est une journée (date) il faut la transformer en datetime aumême jour minuit
                    #begin_date = datetime.combine(e.get('dtend').dt, datetime.min.time())
                    pass
                # Hack pour les événements récurrents (paramètre RRULE du strftime ics)
                if "RRULE" in e.keys():
                    rrule = e.get('RRULE').to_ical().decode("utf-8")
                    begin_date = e.get('dtstart').dt
                    rule = dateutil.rrule.rrulestr(rrule, dtstart=begin_date)
                    next = rule.after(now)
                    if next is not None and next > now:
                        event = (next, e.get('summary'))
                        events.append(event)
                    elif next is not None and next < now:
                        event = (next, e.get('summary'))
                        past_events.append(event)
    
            events.sort(key=lambda e: e[0])
            past_events.sort(key=lambda e: e[0])
            if events:
                return (True, events[0])
            else:
                return (False, past_events[-1])
        except:
            # TODO
            raise
