import urllib.request
import urllib.error
import json

from logging import getLogger

from plugin import Plugin

logger = getLogger(__name__)


class YoutubeInfo(Plugin):
    """YoutubeInfo Plugin

    Collect automatically information about videos linked to by querying the
    Google Youtube API (needs an API key)

    Configuration:
       youtube_api_url : URL of the Youtube API endpoint
       youtube_api_key : a valid key for querying the API
    """
    def __init__(self):
        help = 'Info from Youtube'
        name = 'youtube_info'
        Plugin.__init__(self, name, help)
        #self.api_key == 'blob'
        self.regexps = ['https?://(?:www\.)?(?:youtu(?:be\.(?:com|fr)|\.be))/watch\?.*v=([\-_A-Za-z0-9]+)', 'https?://(?:www\.)?(?:youtu\.be)/([\-_A-Za-z0-9]+)']

    def call(self, bot, context, *video_ids):
        for video_id in video_ids:
            v = self.get_youtube_info(video_id)
            answer = "Vidéo : " + v['title']
            bot.connection.privmsg(context['channel'], answer)

    def get_youtube_info(self, video_id):
        """Returns a JSON object with video informations according to the Youtube API

        Args:
            video_id (string): id of the Youtube video
        Returns:
            object : JSON converted to a Python object
        """
        youtube_query = self.config['youtube_api_url'] +  "?part=snippet" + "&key=" + self.config['youtube_api_key'] + "&id=" + video_id
        try:
            video_json = urllib.request.urlopen(youtube_query, timeout=5).read().decode()
            video_obj = json.loads(video_json)['items'][0]['snippet']
            return video_obj
        except urllib.error.HTTPError as e:
            # Network failure or API error
            logger.exception("Failure to get %s from Youtube", video_id,
                             exc_info=e)
            pass
        except:
            # TODO
            raise
