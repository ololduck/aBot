import feedparser
from plugin import Plugin

class AbbePierre(Plugin):
    def __init__(self):
        help = 'Print a picture of Abbé Pierre'
        name = 'abbe_pierre'
        Plugin.__init__(self, name, help)
        self.commands = ['pierre']

    def call(self, bot, context, *args):
        answer = "Tu l'as demandé, le voici : https://upload.wikimedia.org/wikipedia/commons/6/65/ABBE_PIERRE-1999Without.jpg"
        bot.connection.privmsg(context['channel'], answer)
