import urllib.request
import json

from logging import getLogger

from plugin import Plugin

logger = getLogger(__name__)


class DailymotionInfo(Plugin):
    """DailymotionInfo Plugin

    Collect automatically information about videos linked to by querying the
    Dailymotion API endpoint

    Configuration:
       dailymotion_api_url : URL of the Youtube API endpoint
    """
    def __init__(self):
        help = 'Info from Dailymotion'
        name = 'dailymotion_info'
        Plugin.__init__(self, name, help)
        self.regexps = ['https?://(?:www.)?dailymotion.com/video/([A-Za-z0-9]+)_.+']

    def call(self, bot, context, *video_ids):
        for video_id in video_ids:
            v = self.get_dailymotion_info(video_id)
            answer = "Vidéo : " + v['title'] + " (" + explicit_duration(v['duration']) + ")"
            bot.connection.privmsg(context['channel'], answer)

    def get_dailymotion_info(self, video_id):
        """Returns a JSON object with video informations according to the Youtube API

        Args:
            video_id (string): id of the Youtube video
        Returns:
            object : JSON converted to a Python object
        """
        dailymotion_query = self.config['dailymotion_api_url'] + video_id + "?fields=id,title,duration"
        try:
            video_json = urllib.request.urlopen(dailymotion_query).read().decode()
            video_obj = json.loads(video_json)
            return video_obj
        except urllib.error.HTTPError as e:
            # Network failure or API error
            logger.exception("Failure to get %s from Dailymotion", video_id,
                             exc_info=e)
        except:
            # TODO
            raise


def explicit_duration(seconds):
    s = seconds % 60
    minutes = int((seconds - s) / 60)
    m = minutes % 60
    h = int((minutes - m) / 60)
    if h > 0:
        return "{0:02d}:{1:02d}:{2:02d}".format(h, m, s)
    else:
        return "{1:02d}:{2:02d}".format(h, m, s)
