import random
from plugin import Plugin
from carambar_jokes import jokes

class Carambar(Plugin):
    """Carambar Plugin
    
    Says a random joke from the carambar collection
    """
    def __init__(self):
        help = 'Blagues carambar'
        name = 'carambar'
        Plugin.__init__(self, name, help)
        self.commands = ['blague', 'carambar']
        self.jokes = jokes

    def call(self, bot, context, *args):
        joke = random.sample(self.jokes, 1)[0]
        for line in joke:
            bot.connection.privmsg(context['channel'], line)
