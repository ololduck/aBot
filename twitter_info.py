import urllib.request
import json
import re

from logging import getLogger

from plugin import Plugin, PluginException
from bs4 import BeautifulSoup

logger = getLogger(__name__)

class TwitterInfo(Plugin):
    def __init__(self):
        help = 'Info from Twitter'
        name = 'twitter_info'
        Plugin.__init__(self, name, help)
        self.regexps = ['https?://(?:www.)?(?:mobile.)?twitter.com/[A-Za-z0-9\-_]+/status/[0-9]+']

    def call(self, bot, context, *tweet_ids):
        for tweet_id in tweet_ids:
            # Workaround fo rmobile URLs
            tweet_id = tweet_id.replace('mobile.twitter.com', 'twitter.com')
            t = self.get_twitter_info(tweet_id)
            answer = t['author'] +  ": " + t['text']
            answer = ' '.join(answer.splitlines())
            bot.connection.privmsg(context['channel'], answer)

    def get_twitter_info(self, tweet_url):
        try:
            tweet_obj = {}
            tweet_html = urllib.request.urlopen(tweet_url).read().decode()
            logger.debug(tweet_html)
            soup = BeautifulSoup(tweet_html, "html.parser")
            tweet_div = soup.find('div', attrs={'class': 'permalink-tweet'})
            logger.debug(tweet_div)
            tweet_obj['author'] = tweet_div.find('span', attrs={'class': 'username'}).text
            tweet_obj['text'] = tweet_div.find('p', attrs={'class': 'tweet-text'}).text
            tweet_obj['text'] = tweet_obj['text'].replace('pic.twitter.com', 'https://pic.twitter.com')
            tweet_obj['text'] = tweet_obj['text'].replace('http', ' http')
            return tweet_obj
        except:
            # TODO
            raise PluginException
