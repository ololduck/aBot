from plugin import Plugin
import urllib.request
import json
import re
from plugin import Plugin, PluginException
import random

def populate_list():
    clouds = ["Cirrus", "Cirrocumulus", "Cirrostratus", "Altocumulus", "Altostratus", "Stratocumulus", "Stratus", "Cumulus", "Nimbostratus", "Cumulonimbus"]
    return clouds

class Cloud(Plugin):
    def __init__(self):
        help = 'Blague sur des nuages'
        name = 'cloud'
        Plugin.__init__(self, name, help)
        self.commands = []
        self.regexps = [
                       '.*(([Pp]utain)|([Ff]uck)|([Bb]ordel)|([Mm]erde)).*[Cc]loud.*',
                       '.*[Cc]loud.*(([Pp]utain)|([Ff]uck)|([Bb]ordel)|([Mm]erde)).*',
                       ]
        self.authors = ['Un_pseudo', 'Un_pseudal','Un_pseudo_', 'Altay']
        self.clouds = populate_list()

    def call(self, bot, context, *args):
        if context['author'] in self.authors:
            cloud = random.sample(self.clouds, 1)[0]
            answer = "୧( ಠ Д ಠ )୨  {name} !".format(name=cloud)
            bot.connection.privmsg(context['channel'], answer)

