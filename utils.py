from __future__ import division
from datetime import timedelta

# From http://stackoverflow.com/questions/19053707/converting-snake-case-to-lower-camel-case-lowercamelcase 


def snake_to_camel_case(snake_str):
    """
    >>> snake_to_camel_case("hallo_weld")
    halloWeld
    """
    components = snake_str.split('_')
    # We capitalize the first letter of each component except the first one
    # with the 'title' method and join them together.
    return "".join(x.title() for x in components)


def seconds_to_time(seconds):
    """
    >>> seconds_to_time(1)
    '1'
    >>> seconds_to_time(61)
    '1:01'
    >>> seconds_to_time(666)
    '11:06'

    :param seconds:
    :type seconds: Any int representation
    :return: a string representing time
    :rtype: str
    """
    return str(timedelta(seconds=int(seconds))).strip('0:')

