import urllib.request
import json
import re

from logging import getLogger

from plugin import Plugin, PluginException
from bs4 import BeautifulSoup
import html

logger = getLogger(__name__)

class MastodonInfo(Plugin):
    def __init__(self):
        help = 'Info from Mastodon'
        name = 'mastodon_info'
        Plugin.__init__(self, name, help)
        self.regexps = ['https?://.+']

    def call(self, bot, context, *urls):
        for url in urls:
            t = self.get_mastodon_info(url)
            if t is not None:
                answer = t['author'] +  ": " + t['text']
                answer = ' '.join(answer.splitlines())
                bot.connection.privmsg(context['channel'], answer)

    def get_mastodon_info(self, tweet_url):
        try:
            mastodon_obj = {}
            html_source = urllib.request.urlopen(tweet_url).read().decode()
            soup = BeautifulSoup(html_source, "html.parser")
            meta_mastodon = soup.find('meta')
            is_mastodon = soup.find('a', attrs={'href': 'https://github.com/tootsuite/mastodon'})
            if is_mastodon:
                mastodon_obj['author'] = meta_mastodon.find('meta', attrs={'property': 'og:title'}).attrs['content']
                hidden = soup.find('a', attrs={'class': 'status__content__spoiler-link'})
                mastodon_obj['text'] = meta_mastodon.find('meta', attrs={'property': 'og:description'}).attrs['content']
                logger.debug(hidden)
                if hidden:
                    hidden_div = soup.find('div', attrs={'class': 'e-content'})
                    hidden_text = hidden_div.find('p').text
                    mastodon_obj['text'] += " " + hidden_text
                logger.debug(mastodon_obj['text'])
                mastodon_obj['text'] = html.unescape(mastodon_obj['text'])
                return mastodon_obj
            else:
                return None
        except:
            # TODO
            raise PluginException

#if __name__ == "__main__":
#    urls = [
#     "https://octodon.social/users/Un_pseudal/updates/24162",
#     "https://mastodon.social/@n0m1s/1877886",
#     "https://social.lou.lt/users/Whidou/updates/6273",
#     "https://mastodon.social/@elecray7k/1877899"
#    ]
#    for url in urls:
#        try:
#            get_mastodon_info(url)
#        except:
#            print("Erreur")
