import urllib.request
import re
from bs4 import BeautifulSoup
from logging import getLogger

import utils
from plugin import Plugin

logger = getLogger(__name__)


class BandcampInfo(Plugin):

    @classmethod
    def get_album_info(cls, url):
        page_content = urllib.request.urlopen(url).read().decode()
        bs = BeautifulSoup(page_content, "html.parser")
        return {
            "album_name": bs.find('h2', attrs={'class': 'trackTitle'}).text,
            "artist": bs.find('span', attrs={'itemprop': 'byArtist'}).find(
                'a').text,
            "cover_url": bs.find('div', attrs={'id': 'tralbumArt'}).find('a', attrs={'class': 'popupImage'}, href=True)['href'],
            "tags": ', '.join([tag.text for tag in bs.find_all('a', attrs={
                'class': 'tag'})])
        }

    @classmethod
    def get_track_info(cls, url):
        page_content = urllib.request.urlopen(url).read().decode()
        bs = BeautifulSoup(page_content, "html.parser")
        return {
            "track_name": bs.find('h2', attrs={'class': 'trackTitle'}).text,
            "album_name": bs.find('span', attrs={'itemprop': 'inAlbum'}).find(
                'span', attrs={'itemprop': 'name'}).text,
            "artist": bs.find('span', attrs={'itemprop': 'byArtist'}).find(
                'a').text,
            "duration": utils.seconds_to_time(float(bs.find('meta', attrs={'itemprop': 'duration'})['content'])),
            "cover_url": bs.find('div', attrs={'id': 'tralbumArt'}).find('a',
                                                                         attrs={'class': 'popupImage'},
                                                                         href=True)['href'],
            "tags": ', '.join(
                [tag.text for tag in bs.find_all('a', attrs={
                    'class': 'tag'})])
        }

    def __init__(self):
        help = 'Info from bandcamp tracks or albums'
        name = 'bandcamp_info'
        super().__init__(name, help)
        self.regexps = [
            'https?://[a-z0-9\-]+\.bandcamp.com/$',
            'https?://[a-z0-9\-]+\.bandcamp.com/track/.+',
            'https?://[a-z0-9\-]+\.bandcamp.com/album/.+'
        ]

    def call(self, bot, context, *bandcamp_urls):
        """
        Method called by the main bot to perform the action. This is the
        entrypoint of the plugin


        >>> # begin monkey-patching
        >>> class Bot(object):
        ...     class Connection(object):
        ...         def privmsg(self, *args):
        ...             print("{0}: {1}".format(*args))
        ...     def __init__(self):
        ...         self.connection = Bot.Connection()
        ...
        >>> b = Bot()
        >>> context = {'channel': '#test'}
        >>> def void(*args):
        ...     return
        ...
        >>> BandcampInfo.__init__ = void
        >>> # end monkey-patching
        >>> bandcamp = BandcampInfo()
        >>> bandcamp.call(b, context, "https://mutoidman.bandcamp.com/track/date-with-the-devil")
        #test: Bandcamp : Date with the Devil (0:02:53) from War Moans by Mutoid Man (tags: rock, metal, punk, New York) https://f4.bcbits.com/img/a3994329067_10.jpg
        >>> bandcamp.call(b, context, "https://mutoidman.bandcamp.com/album/war-moans")
        #test: Bandcamp : War Moans by Mutoid Man (tags: rock, metal, punk, New York) https://f4.bcbits.com/img/a3994329067_10.jpg
        """
        for url in bandcamp_urls:
            if 'track' in url:
                info = self.get_track_info(url)
                answer = "{track_name} ({duration}) from {album_name} by {" \
                         "artist} (tags: " \
                         "{tags}) {cover_url}".format(**info)
                logger.info("Bandcamp track : " + answer)
            else:
                info = self.get_album_info(url)
                answer = "{album_name} by {artist} (tags: {tags}) {" \
                         "cover_url}".format(**info)
                logger.info("Bandcamp album : " + answer)
            answer = "Bandcamp : " + answer
            answer = ' '.join(answer.splitlines())
            answer = re.sub(' +', ' ', answer)
            bot.connection.privmsg(context['channel'], answer)
