import urllib.request
import json
import sys
sys.path.insert(0, '..')
from plugin import Plugin
from bs4 import BeautifulSoup

def common_parse(self, html):
    soup = BeautifulSoup(html, 'html.parser')
    hour = soup.find('div', { "id": "clock" }).text.replace('\n','')
    message = ' '.join(soup.find('p').text.splitlines())
    #gif_url = url + soup.find('img')['src'][2:]
    answer = hour + " ? " + message #+ gif_url
    return answer

"""
Generic class for the webparsers
"""
class WebParser(Plugin):
    def __init__(self, name, help, url, commands, html_parser=common_parse):
        self.url = url
        Plugin.__init__(self, name, help)
        self.regexps = []
        self.commands = commands
        self.parse_html = html_parser

    def call(self, bot, context, *args):
        html = urllib.request.urlopen(self.url).read().decode()
        answer = self.parse_html(self, html)
        bot.connection.privmsg(context['channel'], answer)
