from web_parser.web_parser import WebParser
from bs4 import BeautifulSoup

"""
EnProd
"""
def Prod():
    def parser(self, html):
        soup = BeautifulSoup(html, 'html.parser')
        day = soup.find('h1').text
        gif_url = self.url + soup.find('img')['src']
        text = soup.find('p').text
        answer = day + " " + text + " " + gif_url
        answer = ' '.join(answer.splitlines())
        return answer
    help = "Est-ce qu'on met en prod aujourd'hui ?"
    name = 'web_parser.prod'
    url = "http://www.estcequonmetenprodaujourdhui.info"
    commands = ['prod']
    return WebParser(name, help, url, commands, html_parser=parser)
