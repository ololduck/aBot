import yaml
import sys

from logging import getLogger

logger = getLogger(__name__)


class PluginException(Exception):
    pass


class Plugin():
    """
    name : le nom du module (unique)
    commands : différents alias pour appeler ce module
    help : texte de documentation
    """
    def __init__(self, name, help):
        self.name = name
        self.commands = []
        self.regexps = []
        self.help_text = help
        conf = yaml.load(open('config.yml'))
        self.config = conf.get(self.name, {})

    def process(self, bot, context, *args):
        try:
            self.call(bot, context, *args)
        except Exception as e:
            logger.exception("Error in module %s:", self.name,
                             exc_info=e)

    def call(self, bot, context, *args):
        # Should be specialized
        raise NotImplementedError
