import irc.bot
import re

import yaml
from jaraco.functools import Throttler
import importlib

from logging import getLogger, basicConfig, DEBUG

import utils

# Try to fallback to ISO-8859 when UTF-8 fails
from jaraco.stream import buffer 
irc.client.ServerConnection.buffer_class = buffer.LenientDecodingLineBuffer

CONFIG_FILE = 'config.yml'

logger = getLogger(__name__)
basicConfig(level=DEBUG)


class ABot(irc.bot.SingleServerIRCBot):
    def reload_plugins(self):
        """ Drop all currently loaded plugins and reload based on the config file. """
        # Load the configuration file
        config = self.get_config(CONFIG_FILE)
        plugins = config['plugins']

        # Get the name for each plugin
        current_plugins = [m.__name__ for _, m in self.loaded_plugins.items()]
        # Compute the new plugins that we have to import
        new_plugins = list(set(plugins) - set(current_plugins))

        # TODO : remove obsolete plugins

        # Recreate the plugins and import the new ones
        self.load_plugins(plugins, import_modules=new_plugins, reload_old=True)

    def load_plugins(self, plugins, import_modules=[], reload_old=False):
        """ Load plugins into the bot by importing them if needed. 
        Args :
            plugins : list of plugin names (same as the config file)
            import_modules : names of the modules we have to import in Python
            reload_old : set to True if we want to re-import the old modules
        """
        if reload_old:
        # Reload already loaded modules
            for name, module in self.loaded_plugins.items():
                importlib.reload(module)

        old_plugins = self.plugins
        # Reset the plugins attribute
        self.plugins = []

        # Import the modules in Python using importlib
        for module_name in import_modules:
            module = importlib.import_module(module_name)
            self.loaded_plugins[module_name] = module
            logger.info(module_name + " loaded")
        # Instantiate the Module object
        for plugin_name in plugins:
            plugin = self.loaded_plugins[plugin_name]
            # The class name is derived by switching to CamelCase
            plugin_class_name = utils.snake_to_camel_case(plugin.__name__).split('.')[-1]
            plugin_class = getattr(plugin, plugin_class_name)
            self.plugins.append(plugin_class())
        logger.debug(self.plugins)

        # Pre-compile the regexps
        for plugin in self.plugins:
            plugin.regexps = [ re.compile(reg) for reg in plugin.regexps]

    @staticmethod
    def get_config(config_filename):
        """ Load the Yaml configuration file based on the filename. """
        f = open(config_filename, 'r')
        config = yaml.load(f)
        f.close()
        return config

    def __init__(self):
        """ Init the bot by reading the configuration and setting the parameters. """
        config = self.get_config(CONFIG_FILE)

        # Imported plugins
        self.loaded_plugins = {}
        # Instances of loaded Module objects
        self.plugins = []
        # Let's import the modules in Python and instantiate the plugins
        self.load_plugins(config['plugins'], import_modules=config['plugins'])

        # Set the connection parameters
        server_url = config['server'].get('url', None)
        server_port = config['server'].get('port', 6667)
        self.nickname = config['bot'].get('nickname', 'a_weird_bot')
        self.realname = config['bot'].get('realname', 'A weird bot')
        self.start_channels = config['server'].get('channels', [])

        # Start the bot
        irc.bot.SingleServerIRCBot.__init__(self, [(server_url, server_port)], self.nickname, self.realname)
        
        # Set the global throttling
        global_throttle = config.get('throttle', {}).get('global', None)
        if global_throttle and global_throttle > 0:
            self.connection.set_rate_limit(global_throttle)
        # Throttle messaging to 3 messages/second to avoid kicks and bans
        messaging_throttle = config.get('throttle', {}).get('messaging', 3)
        if messaging_throttle > 0 and messaging_throttle:
            self.connection.privmsg = Throttler(self.connection.privmsg, messaging_throttle)

    def on_welcome(self, server, event):
        """ Join the channels after the connection. """
        for channel in self.start_channels:
            server.join(channel)

    def on_pubmsg(self, server, event):
        """ Parse the public messages that are received. """
        context = {
            'author': event.source.nick,
            'channel': event.target,
            'message': event.arguments[0]
        }
        self.process_msg(context)

    def on_privmsg(self, server, event):
        """ Parse the private messages that are received. """
        context = {
            'author': event.source.nick,
            'channel': event.source.nick,
            'message': event.arguments[0]
        }
        self.process_msg(context)

    def process_msg(self, context):
        """ Process a message, either by matching a regexp or by finding a specific command. """
        for plugin in self.plugins:
            for command in plugin.commands:
                # Commands follow the syntax "!ab command"
                if context['message'].startswith("!ab " + command):
                    # Pass the args in a UNIX way (args[0] is the command name, etc.)
                    plugin.process(self, context, *context['message'].split(" ")[1:])
            for regexp in plugin.regexps:
                # Try to match a regexp
                args = regexp.findall(context['message'])
                if args:
                    plugin.process(self, context, *args)
                    break

if __name__ == '__main__':
    bot = ABot()
    # Start the bot
    bot.start()
