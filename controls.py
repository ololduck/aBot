from plugin import Plugin, PluginException

class Controls(Plugin):
    """Control Plugin
    
    Provides a public control interface for the bot
    """
    def __init__(self):
        help = 'Controlling the bot'
        name = 'controls'
        Plugin.__init__(self, name, help)
        self.functions_ = {
                "help": help_,
                "version": version_,
                "plugins": plugins_
        }
        self.commands = self.functions_.keys()


    def call(self, bot, context, *args):
        if len(args) < 1:
            # Something went wrong
            raise PluginException("Too many arguments")
        else:
            answer = self.functions_[args[0]](bot, context, *args[1:])
            bot.connection.privmsg(context['channel'], answer)

# TODO : transform into @static ?
def help_(bot, context, *args):
    answer = ""
    #for plugin in bot.plugins:
    #    answer += plugin.help + "\n"
    answer = "Moi j'suis aBot, et j'suis là pour aider."
    return answer

def version_(bot, context, *args):
    answer = "aBot, version 0.0.1-dev"
    return answer

def plugins_(bot, context, *args):
    plugins = map(lambda m: m.name, bot.plugins)
    answer = ", ".join(plugins)
    return answer
